export class Book {
  id?: number;
  title?: string;
  author?: string;
  note?: string;
  publisher?: string;
  isbn?: string;
  pages?: number;
  genre?: string;
  language?: string;
  edition?: number;
  publicationDate?: Date;
  coverImage?: string;
  summary?: string;
  rating?: number;
  lastModificationDate?: Date;
}
