import { Component, OnInit } from '@angular/core';
import { BookService } from '../shared/service/book.service';
import { Book } from '../shared/model/book.model';
import { ConfirmationService, MessageService } from 'primeng/api';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  books: Book[] = [];
  selectedBook!: Book;
  displayDialog: boolean = false;
  bookForm!: FormGroup;

  constructor(
    private bookService: BookService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.loadBooks();
    this.bookForm = new FormGroup({
      'id': new FormControl(0),
      'title': new FormControl(null, Validators.required),
      'author': new FormControl(null, Validators.required),
      'note': new FormControl(null),
      'publisher': new FormControl(null),
      'isbn': new FormControl(null),
      'pages': new FormControl(null, [Validators.min(1)]),
      'genre': new FormControl(null),
      'language': new FormControl(null),
      'edition': new FormControl(null),
      'publicationDate': new FormControl(null),
      'coverImage': new FormControl(null),
      'summary': new FormControl(null),
      'rating': new FormControl(null, [Validators.min(0), Validators.max(5)]),
      'lastModificationDate': new FormControl(null)
    });
  }

  loadBooks(): void {
    this.bookService.getAllBooks().subscribe(books => {
      this.books = books;
    });
  }

  onCoverImageChange(event :any) {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = (e) => {
      this.bookForm.get('coverImage')?.setValue(reader.result!.toString());
    };

    reader.readAsDataURL(file);
  }



  editBook(book: Book): void {
    this.selectedBook = { ...book };
    this.displayDialog = true;
  }

  deleteBook(id: number): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this book?',
      accept: () => {
        this.bookService.deleteBook(id).subscribe(() => {
          this.loadBooks();
          this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Book deleted successfully' });
        });
      }
    });
  }

  createBook(): void {
    // Logique de mise à jour ou de création
    this.bookService.createBook(this.bookForm.value).subscribe(updatedBook => {
      this.loadBooks();
      this.displayDialog = false;
      this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Book updated successfully' });
    });
  }


}
