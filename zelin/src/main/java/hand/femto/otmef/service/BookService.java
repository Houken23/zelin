package hand.femto.otmef.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

import hand.femto.otmef.domain.Book;
import hand.femto.otmef.repository.BookRepository;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public Book getBookById(Long id) throws NotFoundException {
        return bookRepository.findById(id).orElseThrow(() -> new NotFoundException());
    }

    public Book createBook(Book book) {
        return bookRepository.save(book);
    }

    public Book updateBook(Long id, Book bookDetails) throws NotFoundException {
        Book book = getBookById(id);
        book.setTitle(bookDetails.getTitle());
        book.setAuthor(bookDetails.getAuthor());
        book.setNote(bookDetails.getNote());
        book.setLastModificationDate(LocalDate.now());
        return bookRepository.save(book);
    }

    public void deleteBook(Long id) throws NotFoundException {
        Book book = getBookById(id);
        bookRepository.delete(book);
    }
}
