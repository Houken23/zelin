package hand.femto.otmef.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hand.femto.otmef.domain.Book;
import hand.femto.otmef.service.BookService;

@RestController
@RequestMapping("/api/books")
@CrossOrigin(origins = "*")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping
    @CrossOrigin(origins = "*")
    public List<Book> getAllBooks() {
        return bookService.getAllBooks();
    }

    @GetMapping("/{id}")
    @CrossOrigin(origins = "*")
    public Book getBookById(@PathVariable Long id) throws NotFoundException {
        return bookService.getBookById(id);
    }

    @PostMapping
    @CrossOrigin(origins = "*")
    public Book createBook(@RequestBody Book book) {
        return bookService.createBook(book);
    }

    @PutMapping("/{id}")
    @CrossOrigin(origins = "*")
    public Book updateBook(@PathVariable Long id, @RequestBody Book bookDetails) throws NotFoundException {
        return bookService.updateBook(id, bookDetails);
    }

    @DeleteMapping("/{id}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> deleteBook(@PathVariable Long id) throws NotFoundException {
        bookService.deleteBook(id);
        return ResponseEntity.ok().build();
    }
}

