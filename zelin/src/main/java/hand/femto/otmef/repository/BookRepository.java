package hand.femto.otmef.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hand.femto.otmef.domain.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
}

