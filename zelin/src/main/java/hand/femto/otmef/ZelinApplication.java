package hand.femto.otmef;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZelinApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZelinApplication.class, args);
	}

}
