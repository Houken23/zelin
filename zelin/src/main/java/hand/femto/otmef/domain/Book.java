package hand.femto.otmef.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;




@Entity
public class Book {

	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;
	    private String title;
	    private String author;
	    private String note;
	    private String publisher;
	    private String isbn;
	    private Integer pages;
	    private String genre;
	    private String language;
	    private Integer edition;
	    private LocalDate publicationDate;	    
	    @Column(columnDefinition = "TEXT")
	    private String coverImage; 	    
	    private String summary;
	    private Integer rating;	 
	    private LocalDate lastModificationDate;
	    
	    
	    
	    
	    
	    
		public Book(Long id, String title, String author, String note, String publisher, String isbn, Integer pages,
				String genre, String language, Integer edition, LocalDate publicationDate, String coverImage,
				String summary, Integer rating, LocalDate lastModificationDate) {
			super();
			this.id = id;
			this.title = title;
			this.author = author;
			this.note = note;
			this.publisher = publisher;
			this.isbn = isbn;
			this.pages = pages;
			this.genre = genre;
			this.language = language;
			this.edition = edition;
			this.publicationDate = publicationDate;
			this.coverImage = coverImage;
			this.summary = summary;
			this.rating = rating;
			this.lastModificationDate = lastModificationDate;
		}
		
		
		
		
		public Book() {
			super();
		}




		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getAuthor() {
			return author;
		}
		public void setAuthor(String author) {
			this.author = author;
		}
		public String getNote() {
			return note;
		}
		public void setNote(String note) {
			this.note = note;
		}
		public String getPublisher() {
			return publisher;
		}
		public void setPublisher(String publisher) {
			this.publisher = publisher;
		}
		public String getIsbn() {
			return isbn;
		}
		public void setIsbn(String isbn) {
			this.isbn = isbn;
		}
		public Integer getPages() {
			return pages;
		}
		public void setPages(Integer pages) {
			this.pages = pages;
		}
		public String getGenre() {
			return genre;
		}
		public void setGenre(String genre) {
			this.genre = genre;
		}
		public String getLanguage() {
			return language;
		}
		public void setLanguage(String language) {
			this.language = language;
		}
		public Integer getEdition() {
			return edition;
		}
		public void setEdition(Integer edition) {
			this.edition = edition;
		}
		public LocalDate getPublicationDate() {
			return publicationDate;
		}
		public void setPublicationDate(LocalDate publicationDate) {
			this.publicationDate = publicationDate;
		}
		public String getCoverImage() {
			return coverImage;
		}
		public void setCoverImage(String coverImage) {
			this.coverImage = coverImage;
		}
		public String getSummary() {
			return summary;
		}
		public void setSummary(String summary) {
			this.summary = summary;
		}
		public Integer getRating() {
			return rating;
		}
		public void setRating(Integer rating) {
			this.rating = rating;
		}
		public LocalDate getLastModificationDate() {
			return lastModificationDate;
		}
		public void setLastModificationDate(LocalDate lastModificationDate) {
			this.lastModificationDate = lastModificationDate;
		};

   
}

